let s:name = expand('<sfile>')

function! synfreeze#undo(...) abort
    call nvim_buf_clear_namespace(0, nvim_create_namespace(s:name), 0, -1)
    if !empty(&l:syntax)
        let &l:syntax=&l:syntax
    endif
    echo "SynFreeze: undone"
endfunction

function! synfreeze#do(...) abort
    if a:0
        let l0 = a:1
    else
        let l0 = line('.') - 100
    endif
    let l0 = max([1, l0])
    let n = nvim_create_namespace(s:name)
    "call nvim_buf_clear_namespace(0, n, 0, line('$'))
    let cache = {}
    let L = line('$')
    let l = l0
    let j = 0
    let J = L / 20
    while 1
        let pc = 1
        let pi = 0
        for c in range(1,len(getline(l)))
            let i = synID(l, c, v:true)
            "let i = get(synstack(l, c), -1)
            if i != pi
                if pi
                    let h = get(cache, pi)
                    if empty(h)
                        let h = synIDattr(pi, 'name')
                        let cache[pi] = h
                    endif
                    call nvim_buf_add_highlight(0, n, h, l-1, pc-1, c-1)
                endif
                let pc = c
                let pi = i
            endif
        endfor
        if pi
            let h = get(cache, pi)
            if empty(h)
                let h = synIDattr(pi, 'name')
                let cache[pi] = h
            endif
            call nvim_buf_add_highlight(0, n, h, l-1, pc-1, c)
        endif
        let l = l % L + 1
        if !(j%J) | echo printf('SynFreeze: %02d %%', 100*j/L) | redraw | endif
        if l == l0 | break | endif
        let j += 1
    endwhile
    syn clear
    echo 'SynFreeze: done'
endfunction
