command! -bar SynFreeze call synfreeze#do()
command! -bar SynUnFreeze call synfreeze#undo()
nnoremap <silent> <F1><F1> :<C-U>SynFreeze<CR>
nnoremap <silent> <F1><F2> :<C-U>SynUnFreeze<CR>
